package com.thelinky.donggun.practice;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * A placeholder fragment containing a simple view.
 */
public class MovieDetailFragment extends Fragment {
    private TextView title;
    private TextView synopsis;
    private ImageView poster;
    private TextView release_date;
    private TextView rating;

    public MovieDetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_movie_detail, container, false);

        title = (TextView) rootView.findViewById(R.id.detailTitleTextView);
        synopsis = (TextView) rootView.findViewById(R.id.detailSynopsisTextView);
        poster = (ImageView) rootView.findViewById(R.id.detailPosterImageView);
        release_date = (TextView) rootView.findViewById(R.id.detailReleaseYear);
        rating = (TextView) rootView.findViewById(R.id.detailUserRating);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle b = getActivity().getIntent().getExtras();

        String titleValue = b.getString("title");
        String synopsisValue = b.getString("synopsis");
        String poster_pathValue = b.getString("poster_path");
        String release_dateValue = b.getString("release_date");
        String release_year = "";
        release_year = release_dateValue.substring(0, release_dateValue.indexOf("-"));
        Double vote_averageValue = b.getDouble("vote_average");

        title.setText(titleValue);
        synopsis.setText(synopsisValue);
        Picasso.with(getActivity()).load("http://image.tmdb.org/t/p/w185//" + poster_pathValue).placeholder(R.drawable.loading).into(poster);
        release_date.setText(release_year);
        rating.setText(vote_averageValue + "/10");

    }
}
