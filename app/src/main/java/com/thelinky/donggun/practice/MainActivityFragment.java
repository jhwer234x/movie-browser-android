package com.thelinky.donggun.practice;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    CustomAdapter adapter;
    // BOOLEAN TO CHECK IF NEW FEEDS ARE LOADING
    Boolean loadingMore = true;
    Boolean stopLoadingData = false;
    private int currentPosition = 0;
    private ArrayList<Movie> rowItems = new ArrayList<>();
    private GridView gv;
    private int pageNum = 1;

    public MainActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        gv = (GridView) rootView.findViewById(R.id.gridView);
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), MovieDetail.class);
                Bundle b = new Bundle();
                b.putString("title", adapter.getItem(position).getTitle());
                b.putString("synopsis", adapter.getItem(position).getOverview());
                b.putString("poster_path", adapter.getItem(position).getPoster_path());
                b.putString("release_date", adapter.getItem(position).getRelease_date());
                b.putDouble("vote_average", adapter.getItem(position).getVote_average());
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        // ONSCROLLLISTENER
        gv.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && !(loadingMore)) {

                    if (!stopLoadingData) {
                        // FETCH THE NEXT BATCH OF FEEDS
                        FetchMoviesTask moviesTask = new FetchMoviesTask();
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                        String sort = prefs.getString(getString(R.string.pref_sort_key),
                                getString(R.string.pref_sort_label_popular));
                        moviesTask.execute(sort, pageNum++ + "");
                    }

                }
            }
        });

        FetchMoviesTask moviesTask = new FetchMoviesTask();

        //first is preference of sort
        //second is pageNum
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String sort = prefs.getString(getString(R.string.pref_sort_key),
                getString(R.string.pref_sort_label_popular));
        moviesTask.execute(sort, pageNum++ + "");

        adapter = new CustomAdapter(getActivity(), rowItems);
        gv.setAdapter(adapter);
        gv.setSelection(currentPosition + 1);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivityForResult(new Intent(getActivity(), SettingsActivity.class), 1);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {

            Log.d("resultCode: ", String.valueOf(resultCode));
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                if (result.equals("1")) {
                    // FETCH THE NEXT BATCH OF FEEDS
                    FetchMoviesTask moviesTask = new FetchMoviesTask();
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    String sort = prefs.getString(getString(R.string.pref_sort_key),
                            getString(R.string.pref_sort_label_popular));
                    adapter.clear();
                    pageNum = 1;
                    moviesTask.execute(sort, pageNum++ + "");
                }
            }
        }
    }

    private class FetchMoviesTask extends AsyncTask<String, Void, ArrayList<Movie>> {

        private final String LOG_TAG = FetchMoviesTask.class.getSimpleName();

        @Override
        protected ArrayList<Movie> doInBackground(String... params) {
            loadingMore = true;
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            // Will contain the raw JSON response as a string.
            String movieJsonStr = null;

            try {
                // Construct the URL for the OpenWeatherMap query
                // Possible parameters are avaiable at OWM's forecast API page, at
                // http://openweathermap.org/API#forecast
                final String FORECAST_BASE_URL =
                        "http://api.themoviedb.org/3/discover/movie?";
                final String SORT_PARAM = "sort_by";
                final String PAGE_NUM_PARAM = "page";
                final String API_PARAM = "api_key";

                Uri builtUri;
                if (params[0].equals("vote_average.desc")) {
                    builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                            .appendQueryParameter(SORT_PARAM, params[0])
                            .appendQueryParameter(PAGE_NUM_PARAM, params[1])
                            .appendQueryParameter("vote_count.gte", "200")
                            .appendQueryParameter(API_PARAM, "89caebb7be1d89629369149aa6d0df88")
                            .build();
                } else {
                    builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                            .appendQueryParameter(SORT_PARAM, params[0])
                            .appendQueryParameter(PAGE_NUM_PARAM, params[1])
                            .appendQueryParameter(API_PARAM, "89caebb7be1d89629369149aa6d0df88")
                            .build();
                }

                URL url = new URL(builtUri.toString());

                // Create the request to OpenWeatherMap, and open the connection
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    return null;
                }
                movieJsonStr = buffer.toString();
            } catch (IOException e) {
                Log.e(LOG_TAG, "Error ", e);
                // If the code didn't successfully get the weather data, there's no point in attemping
                // to parse it.
                return null;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }
            Log.d("json", movieJsonStr);
            try {
                return getMovieDataFromJson(movieJsonStr);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Movie> result) {
            // get listview current position - used to maintain scroll position
            currentPosition = gv.getFirstVisiblePosition();

            if (result != null) {
                rowItems.addAll(result);
                // New data is back from the server.  Hooray!

                // Setting new scroll position
                gv.setSelection(currentPosition + 1);

                loadingMore = false;
            }
        }

        private ArrayList<Movie> getMovieDataFromJson(String jsonStr) throws JSONException {
            // These are the names of the JSON objects that need to be extracted.
            final String OWM_ID = "id";
            final String OWM_LIST = "results";
            final String OWM_TITLE = "original_title";
            final String OWM_SYNOPSIS = "overview";
            final String OWM_RELEASE_DATE = "release_date";
            final String OWM_POSTER_PATH = "poster_path";
            final String OWM_VOTE = "vote_average";

            JSONObject movieJson = new JSONObject(jsonStr);
            JSONArray movieArray = movieJson.getJSONArray(OWM_LIST);

            ArrayList<Movie> resultMovies = new ArrayList<>();

            for (int i = 0; i < movieArray.length(); i++) {
                int id;
                String title;
                String synopsis;
                String release_date;
                String poster_path;
                Double vote;

                JSONObject movieInfo = movieArray.getJSONObject(i);
                id = movieInfo.getInt(OWM_ID);
                title = movieInfo.getString(OWM_TITLE);
                synopsis = movieInfo.getString(OWM_SYNOPSIS);
                release_date = movieInfo.getString(OWM_RELEASE_DATE);
                poster_path = movieInfo.getString(OWM_POSTER_PATH);
                vote = movieInfo.getDouble(OWM_VOTE);
                resultMovies.add(new Movie(id, poster_path, title, synopsis, release_date, vote));
            }

            return resultMovies;
        }
    }
}
