package com.thelinky.donggun.practice;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {
    Context context;
    ArrayList<Movie> rowItem;

    public CustomAdapter(Context context, ArrayList<Movie> rowItem) {
        this.context = context;
        this.rowItem = rowItem;
    }

    @Override
    public int getCount() {
        return rowItem.size();
    }

    @Override
    public Movie getItem(int position) {
        return rowItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_item, null);
        }

        ImageView image = (ImageView) convertView.findViewById(R.id.imageView);

        Picasso.with(context).cancelRequest(image);
        // setting the image resource
        Picasso.with(context).load("http://image.tmdb.org/t/p/w185//" + rowItem.get(position).poster_path).placeholder(R.drawable.loading).into(image);

        return convertView;
    }

    public void clear() {
        for (int i = 0; i < rowItem.size(); i++) {
            rowItem.clear();
        }
        notifyDataSetChanged();
    }
}
